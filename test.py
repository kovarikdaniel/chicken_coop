from time import sleep
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(37, GPIO.OUT)

pwm = GPIO.PWM(37, 100) 
pwm.start(0) 
print(GPIO.input(37))
try:
    while 1:                    # Loop will run forever
        for x in range(0, 100, 5):    # This Loop will run 100 times
            pwm.ChangeDutyCycle(x) # Change duty cycle
            sleep(0.5)
            print(GPIO.input(37), x)       # Delay of 10mS
            
        for x in range(100,0,-5): # Loop will run 100 times; 100 to 0
            pwm.ChangeDutyCycle(x)
            sleep(0.5)
            print(GPIO.input(37), x)
except KeyboardInterrupt:
    pass

pwm.stop()

GPIO.output(37, False)
print(GPIO.input(37))




