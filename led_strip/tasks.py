import time

from django.utils import timezone

from huey.contrib.djhuey import task, periodic_task
from huey import crontab

from .led_strip import LedStripClass

from .models import LedStrip


from .utils import get_eta, update_status


@task()
def manual_control():
    ledstrip_obj = LedStrip.objects.get(pk=1)
    ledstrip_status = ledstrip_obj.status
    from .led_strip import LedStripClass
    ledstrip = LedStripClass()

    if ledstrip_status in ['on', 'brightening']:
        new_status = 'dimming'
        update_status(new_status)
        ledstrip.off(step_seconds=0.5)
    else:
        new_status = 'brightening'
        update_status(new_status)
        ledstrip.on(step_seconds=0.5)


@periodic_task(crontab(minute='*/1'))
def dispatch_led_light():
    ledstrip = LedStrip.objects.get(pk=1)
    next_on = ledstrip.next_on
    next_off = ledstrip.next_off
    now = timezone.now()


    if next_on is None:
        ledstrip.next_on = get_eta('on')
        ledstrip.save(update_fields=['next_on'])
    if next_off is None:
        ledstrip.next_off = get_eta('off')
        ledstrip.save(update_fields=['next_off'])

    try:
        if now > next_off:
            ledstrip.last_off = next_off
            ledstrip.next_off = get_eta('off')
            automatic_off()
        if now > next_on:
            ledstrip.last_on = next_on
            ledstrip.next_on = get_eta('on')
            automatic_on()
        ledstrip.save()
    except TypeError as e:
        print(f'Schedule time is not set. {e}')
    


@task()
def automatic_on():
    ledstrip = LedStripClass()
    ledstrip.on()


@task()
def automatic_off():
    ledstrip = LedStripClass()
    ledstrip.off()