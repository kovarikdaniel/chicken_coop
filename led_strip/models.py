from django.db import models
from django.db.models.fields import CharField
from django.db.models.fields.related import ForeignKey

# Create your models here.

LED_STATUSES = [
        ('on', 'On'),
        ('off', 'Off'),
        ('brightening', 'Brightening'),
        ('dimming', 'Dimming'),
    ]


class LedStrip(models.Model):
    status = models.CharField(max_length=255, choices=LED_STATUSES)
    last_on = models.DateTimeField(blank=True, null=True)
    last_off = models.DateTimeField(blank=True, null=True)
    next_on = models.DateTimeField(blank=True, null=True)
    next_off = models.DateTimeField(blank=True, null=True)
