from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from led_strip import views

urlpatterns = [
    path('<int:pk>/', views.LedStripDetail.as_view()),
    path('light/<int:pk>/', views.LedStripLight.as_view()),
    path('hw-check/', views.LedStripHWCheck.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)