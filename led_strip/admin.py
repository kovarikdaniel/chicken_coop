from django.contrib import admin

from .models import LedStrip

# Register your models here.
@admin.register(LedStrip)
class LedStripAdmin(admin.ModelAdmin):
    pass
