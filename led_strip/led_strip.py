import time

import RPi.GPIO as GPIO

from django.conf import settings

from .utils import update_status


class LedStripClass:
    def __init__(self, *args, **kwargs):
        self.red = settings.RED
        self.green = settings.GREEN
        self.blue = settings.BLUE
        self.frequency = settings.LED_FREQUENCY
        self.dimming_seconds = settings.DIMMING_SECONDS
        self.__init_gpio()

    def __init_gpio(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup([self.red, self.green, self.blue], GPIO.OUT)
        self.pwmR = GPIO.PWM(self.red, self.frequency)
        self.pwmG = GPIO.PWM(self.green, self.frequency)
        self.pwmB = GPIO.PWM(self.blue, self.frequency)

    def __change_dc(self, i):
        self.pwmR.ChangeDutyCycle(i)
        self.pwmG.ChangeDutyCycle(i)
        self.pwmB.ChangeDutyCycle(i)

    def __start_pwm(self, i):
        self.pwmR.start(i)
        self.pwmG.start(i)
        self.pwmB.start(i)

    def __stop_pwm(self, bool_pwm):
        self.pwmR.stop()
        self.pwmG.stop()
        self.pwmB.stop()
        self.pwmR = GPIO.output(self.red, bool_pwm)
        self.pwmG = GPIO.output(self.green, bool_pwm)
        self.pwmB = GPIO.output(self.blue, bool_pwm)

    def on(self, step_seconds=18):
        self.__start_pwm(0)
        for i in range(100):
            self.__change_dc(i)
            time.sleep(step_seconds)
        self.__stop_pwm(True)
        update_status('on')

    def off(self, step_seconds=18):
        self.__start_pwm(100)
        for i in range(100,0,-1):
            self.__change_dc(i)
            time.sleep(step_seconds)
        self.__stop_pwm(False)
        update_status('off')

    def hw_check(self):
        status =  {
            'red': GPIO.input(self.red),
            'green': GPIO.input(self.green),
            'blue': GPIO.input(self.blue),
        }

        return status
        
