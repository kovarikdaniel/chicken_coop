from django.utils import timezone
from datetime import timedelta


from .models import LedStrip

from time_manager.utils import get_tommorow_noon


def update_status(status):
    led_strip = LedStrip.objects.get(pk=1)
    led_strip.status = status
    led_strip.save(update_fields=['status'])


def get_eta(direction):
    # Morning
    if direction == 'on':
        noon = get_tommorow_noon()
        light_up_time = noon - timedelta(hours=7)
        return light_up_time
    # Evening
    else:
        noon = get_tommorow_noon()
        light_down_time = noon + timedelta(hours=7)
        return light_down_time


def report_hw_status():
    from .led_strip import LedStripClass

    led_strip = LedStripClass()
    raw_status = led_strip.hw_check()

    print(raw_status)

    return raw_status