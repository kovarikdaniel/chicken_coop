import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.http import Http404


from .models import LedStrip

from .serializers import LedStripSerializer

from .tasks import manual_control
from .utils import report_hw_status

from time_manager.utils import get_tommorow_evening_blue_hour, get_tommorow_morning_blue_hour


class LedStripDetail(APIView):
    def get_object(self, pk):
        try:
            return LedStrip.objects.get(pk=pk)
        except LedStrip.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        ledstrip = self.get_object(pk)
        serializer = LedStripSerializer(ledstrip)
        return Response(serializer.data)


class LedStripLight(APIView):
    def get_object(self, pk):
        try:
            return LedStrip.objects.get(pk=pk)
        except LedStrip.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        manual_control()
        ledstrip = self.get_object(pk)
        serializer = LedStripSerializer(ledstrip)
        return Response(serializer.data)


class LedStripHWCheck(APIView):
    def get(self, request, format=None):
        return Response(json.dumps(report_hw_status()))