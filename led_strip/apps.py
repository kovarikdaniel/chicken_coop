from django.apps import AppConfig


class LedStripConfig(AppConfig):
    name = 'led_strip'
