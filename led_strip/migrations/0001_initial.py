# Generated by Django 3.1.2 on 2020-10-26 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Door',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(choices=[('on', 'On'), ('off', 'Off'), ('brightening', 'Brightening'), ('dimming', 'Dimming')], max_length=255)),
                ('last_open', models.DateTimeField(blank=True, null=True)),
                ('last_close', models.DateTimeField(blank=True, null=True)),
                ('next_open', models.DateTimeField(blank=True, null=True)),
                ('next_close', models.DateTimeField(blank=True, null=True)),
            ],
        ),
    ]
