from rest_framework import serializers
from .models import LedStrip


class LedStripSerializer(serializers.ModelSerializer):
    next_on = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    next_off = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    last_on = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    last_off = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    class Meta:
        model = LedStrip
        fields = '__all__'
