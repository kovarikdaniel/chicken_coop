from django.utils.timezone import datetime
from datetime import timedelta, tzinfo

from astral import LocationInfo, SunDirection
from astral.sun import sun, blue_hour


def setup_location():
    return LocationInfo("Krsice", "Czech Republic", "Europe/Prague", 49.4821350, 14.0833281)

def get_astral(date):
    city = setup_location()
    return sun(city.observer, date=date, tzinfo=city.timezone)

def get_tommorow_noon():
    tommorow = datetime.today().date() + timedelta(days=1)
    return get_astral(tommorow)['noon']

def get_tommorow_sunrise():
    tommorow = datetime.today().date() + timedelta(days=1)
    return get_astral(tommorow)['sunrise']

def get_tommorow_sunset():
    tommorow = datetime.today().date() + timedelta(days=1)
    return get_astral(tommorow)['sunset']

def get_tommorow_noon():
    tommorow = datetime.today().date() + timedelta(days=1)
    return get_astral(tommorow)['noon']


def get_blue_hour(direction):
    city = setup_location()
    tommorow = datetime.today().date() + timedelta(days=1)
    return blue_hour(city.observer, date=tommorow, direction=direction, tzinfo=city.timezone)

def get_tommorow_morning_blue_hour():
    return get_blue_hour(SunDirection.RISING)[0]

def get_tommorow_evening_blue_hour():
    return get_blue_hour(SunDirection.SETTING)[1]