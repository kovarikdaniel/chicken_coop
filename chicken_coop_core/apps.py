from django.apps import AppConfig


class ChickenCoopCoreConfig(AppConfig):
    name = 'chicken_coop_core'
