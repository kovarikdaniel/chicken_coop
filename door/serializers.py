from rest_framework import serializers
from door.models import Door


class DoorSerializer(serializers.ModelSerializer):
    next_open = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    next_close = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    last_open = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    last_close = serializers.DateTimeField(format='%Y-%m-%d %H:%M:%S')
    class Meta:
        model = Door
        fields = '__all__'
