from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from door import views

urlpatterns = [
    path('<int:pk>/', views.DoorDetail.as_view()),
    path('manipulate/<int:pk>/', views.DoorManipulate.as_view()),
    path('hw-check/', views.DoorHWCheck.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)