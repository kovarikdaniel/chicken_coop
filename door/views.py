import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from django.http import Http404


from door.models import Door

from .serializers import DoorSerializer

from door.tasks import manual_control
from door.utils import report_hw_status

from time_manager.utils import get_tommorow_evening_blue_hour, get_tommorow_morning_blue_hour


class DoorDetail(APIView):
    def get_object(self, pk):
        try:
            return Door.objects.get(pk=pk)
        except Door.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        print(get_tommorow_evening_blue_hour())
        print(get_tommorow_morning_blue_hour())
        door = self.get_object(pk)
        serializer = DoorSerializer(door)
        return Response(serializer.data)


class DoorManipulate(APIView):
    def get_object(self, pk):
        try:
            return Door.objects.get(pk=pk)
        except Door.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        manual_control()
        door = self.get_object(pk)
        serializer = DoorSerializer(door)
        return Response(serializer.data)


class DoorHWCheck(APIView):
    def get(self, request, format=None):
        return Response(json.dumps(report_hw_status()))