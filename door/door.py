import time

import RPi.GPIO as GPIO

from django.conf import settings

from .utils import update_status


class DoorClass:
    def __init__(self, *args, **kwargs):
        self.mag_down = settings.MAG_DOWN
        self.mag_up = settings.MAG_UP
        self.motor_up = settings.MOTOR_UP
        self.motor_down = settings.MOTOR_DOWN
        self.__init_gpio()

    def __init_gpio(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(
            [self.mag_down, self.mag_up],
            GPIO.IN,
            pull_up_down=GPIO.PUD_UP
        )
        GPIO.setup([self.motor_down, self.motor_up], GPIO.OUT)

    def __is_closing(self):
        if GPIO.input(self.mag_down):
            return True
        else:
            return False

    def __is_opening(self):
        if GPIO.input(self.mag_up):
            return True
        else:
            return False

    def close(self):
        direction = 'close'
        GPIO.output(self.motor_down, True)

        while self.__is_closing():
            time.sleep(.5)
        else:
            self.stop(f'{direction}d')

    def open(self):
        direction = 'open'
        GPIO.output(self.motor_up, True)

        while self.__is_opening():
            time.sleep(.5)
        else:
            self.stop(direction)

    def stop(self, direction):
        if direction == 'open':
            GPIO.output(self.motor_up, False)
            update_status(direction)

        else:
            GPIO.output(self.motor_down, False)
            update_status(direction)

    def hw_check(self):
        status =  {
            'mag_down': GPIO.input(self.mag_down),
            'mag_up': GPIO.input(self.mag_up),
            'motor_up': GPIO.input(self.motor_up),
            'motor_down': GPIO.input(self.motor_down)
        }

        return status
        
