import time

from django.utils import timezone

from huey.contrib.djhuey import task, periodic_task
from huey import crontab

from .door import DoorClass

from .models import Door


from .utils import get_eta, update_status


@task()
def manual_control():
    door_obj = Door.objects.get(pk=1)
    door_status = door_obj.status
    from .door import DoorClass
    door = DoorClass()

    if door_status in ['open', 'opening']:
        new_status = 'closing'
        update_status(new_status)
        door.close()
    else:
        new_status = 'opening'
        update_status(new_status)
        door.open()


@periodic_task(crontab(minute='*/1'))
def dispatch_door_manipulation():
    door = Door.objects.get(pk=1)
    next_open = door.next_open
    next_close = door.next_close
    now = timezone.now()


    if next_open is None:
        door.next_open = get_eta('open')
        door.save(update_fields=['next_open'])
    if next_close is None:
        door.next_close = get_eta('close')
        door.save(update_fields=['next_close'])

    try:
        if now > next_close:
            door.last_close = next_close
            door.next_close = get_eta('close')
            automatic_close()
        if now > next_open:
            door.last_open = next_open
            door.next_open = get_eta('open')
            automatic_open()
        door.save()
    except TypeError as e:
        print(f'Schedule time is not set. {e}')
    


@task()
def automatic_open():
    door = DoorClass()
    door.open()


@task()
def automatic_close():
    door = DoorClass()
    door.close()