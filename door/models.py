from django.db import models
from django.db.models.fields import CharField
from django.db.models.fields.related import ForeignKey

# Create your models here.

DOOR_STATUSES = [
        ('open', 'Open'),
        ('closed', 'Closed'),
        ('opening', 'Opening'),
        ('closing', 'Closing'),
    ]


class Door(models.Model):
    status = models.CharField(max_length=255, choices=DOOR_STATUSES)
    last_open = models.DateTimeField(blank=True, null=True)
    last_close = models.DateTimeField(blank=True, null=True)
    next_open = models.DateTimeField(blank=True, null=True)
    next_close = models.DateTimeField(blank=True, null=True)
