from django.utils import timezone
from datetime import timedelta


from .models import Door

from time_manager.utils import get_tommorow_morning_blue_hour, get_tommorow_evening_blue_hour


def update_status(status):
    door = Door.objects.get(pk=1)
    door.status = status
    door.save(update_fields=['status'])


def get_eta(direction):
    # Morning
    if direction == 'open':
        return get_tommorow_morning_blue_hour()
    # Evening
    else:
        return get_tommorow_evening_blue_hour()


def report_hw_status():
    from .door import DoorClass

    door = DoorClass()
    raw_status = door.hw_check()

    print(raw_status)

    return raw_status