from django.contrib import admin

from .models import Door

# Register your models here.
@admin.register(Door)
class DoorAdmin(admin.ModelAdmin):
    pass
